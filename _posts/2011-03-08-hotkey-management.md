---
layout: post
title: Hotkey Management
tags: as3 hotkey keyboard keypress signals
excerpt: Identifying a rich keyboard management system developed by DestroyToday.
---

In an earlier post I explained my approach to keycodes in AS3. If you're
looking for a hotkey system rather than a key sequence matching
solution, take a look at [destroytoday's][] [Hotkey][] project on
[github][]. It's very simple to use and piggybacks on Signals to make
the event listening easier. I may make some modifications to my own
keycode reader based on the very smart way he built his.

  [destroytoday's]: //destroytoday.com/blog/ "Destroy Today"
  [Hotkey]: //github.com/destroytoday/destroy-hotkey "Hotkey"
  [github]: //github.com "Github"
