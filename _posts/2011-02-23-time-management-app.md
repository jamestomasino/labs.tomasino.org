---
layout: post
title: Time Management App
tags: as3 design project-management scroll-wheel time-management ux-design
excerpt: A toy project to visually depict time resourcing of projects.
---

<amp-img width="446" height="421" layout="responsive" src="//labs.tomasino.org/assets/images/timemanagement.jpg" alt="Time Management"></amp-img>

While at [Empathy Lab][], I was used as a resource on a lot of different
projects per week. Sometimes my project managers didn't realize how much
they were booking me and I needed to illustrate it for them. This
interface grew out of that.

At one time I had thought of expanding it and hooking it directly into
our time management system rather than a manual XML file, but it never
came to be. Still, it was fun to design. There's even a little scroll
wheel functionality in there!

[Source & Project][]

  [Empathy Lab]: //www.empathylab.com "Empathy Lab"
  [Source & Project]: //github.com/jamestomasino/timemanagement/
    "Source & Project"
