---
layout: post
title: Air Tools (Fools)
tags: design friend joke logo non-professional
excerpt: A silly logo design to make fun of a company I've never heard of.
---

<amp-img width="575" height="410" layout="responsive" src="//labs.tomasino.org/assets/images/airtools.png" alt="Air Fools"></amp-img>

This was a quick logo design for a friend. I'm sure there's a story
behind it, but I'll be damned if I remember it. I really wanted to play
with a deco-style font, and the blue-swirl-poop thing just kind of
happened. Job well done, sorta!

